// Game
#include "game_defs.h"
#include "screen_splash.h"
#include "screen_game.h"


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Screen_Change(U8 s)
{
    if(s == SCREEN_TYPE_SPLASH) {
        Splash_Init();
        Screen_RunFunc = Splash_Update;
    } else if(s == SCREEN_TYPE_GAME) {
        Splash_Quit();
        Game_Init  ();
        Screen_RunFunc = Game_Update;
    }
}
